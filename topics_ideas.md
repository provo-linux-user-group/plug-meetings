# Provo Linux User Group Meeting Topics
Below is a list of possible meetings topics who will present them and when.

### How to Edit
To add a topic you can either clone this repo, make an edit, and push it back; or you can just edit this page via the GitLab web UI and just save the changes directly back to the master branch. When adding a new topic please add it to the end of the file and follow the [Markdown GitHub Table syntax](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#tables). To make the Markdown table more readable, add a space before and after every `|` character (excluding the first and last pipes, they only have spaces on the inside). For fields that are blank, two spaces will be sufficent. 

The table has the following colomns (seperated by `|`).

* Done => Mark with an `X` if topic is presented
* Topic => The title or idea of presentation topic
* Requestor => The name of the person requesting this topic be presented
* Claimbed By => The name of the person who is actually planning on presenting the topic
* Date => The date the topic was presented at a LUG meeting

For simplicity, here is a sample row. Just copy and paste it then update the sample with the relevant topic.
```
|  | _sample topics_ | _your name_ |  |  |
```

## LUG Topics
| Done | Topic | Requestor | Claimed By | Date |
|:----:|:-----:|:---------:|:----------:|:----:|
| X | Running a Google Apps replacement from home with Nextcloud | Michael Holley | Michael Holley | 2020.06.16 |
| X | A Whirlwind Introduction to Alien Technologies | Alpheus Madsen | Alpheus Madsen | 2020.07.21 |
| X | An Introduction to Go | James Simister | James Simister | 2020.08.18 |
| X | Landing Your Ideal Tech Job | James Simister | James Simister | 2020.10.20 |
| | Automounting Remote Home Directories with SSHFS | James Simister | James SImister | 2021.01.19 |
| | Setting up a personal assitant on a Raspberry Pi | Michael Holley |  |  |
| | Why you should leave Ubuntu for OpenSuSE | Larry Dewey | | |
| | How to use the OpenSuSE Build Service | Larry Dewey | | |
| | What is GPG/PGP and how to use it + Key signing party | Michael Holley | | |
| | How to harden you SSH connection + MFA | Michael Holley | | |
| | Running a VPN at home with WireGuard | Michael Holley | | |
| | Setting up a media center with Kodi | Larry Dewey | | |
| | What is Salt Stack and why you should care | Larry Dewey | | |
| | Automating your home systems with Ansible | Michael Holley | | |
| | Containerization without Docker using Podman | Michael Holley | | |
| | How to set up a simple website without paying for hosting | Michael Holley | | |
| | What is a dot file manager and why you should be running one | Michael Holley | | |
