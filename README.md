# PLUG Meetings

The Provo Linux User Group welcomes people who use or are interested in Linux and Open Source Software in the Provo Utah area.

### Membership
There is no membership fee. Just [join the mailing list](https://plug.org/mailman/listinfo/plug) or [Telegram group](https://t.me/joinchat/DKUwX0sHch1DXKNL4fmCnA) and show up at the meetings!

### Meetings
Meetings are typically held on the third Tuesday of every month at 7:00pm at the [UVU Business Resource Center in Orem](https://goo.gl/maps/QkSB3CpT43c8pBaj7).
